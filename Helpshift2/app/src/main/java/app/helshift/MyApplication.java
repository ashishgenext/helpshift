package app.helshift;

import android.app.Application;

import app.helshift.database.DbHelper;
import app.helshift.database.DbManager;


/**
 * Created by Ashish on 12-06-2016.
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        DbManager.initializeInstance(new DbHelper(getApplicationContext(), "myDb.db"));

    }
}
