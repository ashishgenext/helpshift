package app.helshift.database;

/**
 * Created by Ashish on 12-06-2016.
 */
public class KeyValueTable {

    public static final String TABLE_NAME = "key_value";

    public static final String ID = "_id";
    public static final String KEY = "key";
    public static final String VALUE = "value";

    public static final String TABLE_SQL = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NAME + " (" +ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+ KEY + " TEXT," + VALUE + " TEXT " + ");";

}
