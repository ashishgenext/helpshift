package app.helshift.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Ashish.Am.Singh on 14-06-2016.
 */
public class DbOperations {

    public static void insertQuery(String key, String value) {

        DbManager dbManagerInstance = DbManager.getInstance();
        SQLiteDatabase db = dbManagerInstance.openDatabase();

        ContentValues values = new ContentValues();
        values.put(KeyValueTable.KEY,key);
        values.put(KeyValueTable.VALUE, value);
        db.beginTransaction();
        try {
            db.insert(KeyValueTable.TABLE_NAME, null, values);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            dbManagerInstance.closeDatabase();

        }
    }

    public static String readQuery(String key) {

        DbManager dbManagerInstance = DbManager.getInstance();
        SQLiteDatabase db = dbManagerInstance.openDatabase();
        String value = "";
        Cursor cr = null;

        String selectQuery = "SELECT * FROM "+ KeyValueTable.TABLE_NAME+" where "+KeyValueTable.KEY+"=?" ;
        try {
            db.beginTransaction();
            cr = db.rawQuery(selectQuery, new String[]{key});
            while(cr.moveToNext()) {
                value = cr.getString(cr.getColumnIndex(KeyValueTable.VALUE));
            }
            db.setTransactionSuccessful();
        }finally {
            if(cr != null){
                cr.close();
            }
            db.endTransaction();
            dbManagerInstance.closeDatabase();

        }
        return  value ;
    }

    public static void deleteQuery(String key){
        DbManager dbManagerInstance = DbManager.getInstance();
        SQLiteDatabase db = dbManagerInstance.openDatabase();
        String whereClause = KeyValueTable.KEY + "=?" ;
        String[] whereArgs = new String[] { key };
        try{
            db.beginTransaction();
            db.delete(KeyValueTable.TABLE_NAME, whereClause, whereArgs);
            db.setTransactionSuccessful();
        }finally {
            db.endTransaction();
            dbManagerInstance.closeDatabase();

        }

    }

    public static void updateQuery(String key,String value){

        DbManager dbManagerInstance = DbManager.getInstance();
        SQLiteDatabase db = dbManagerInstance.openDatabase();

        ContentValues values = new ContentValues();
        values.put(KeyValueTable.VALUE,value);

        String where = KeyValueTable.KEY + "=?";

        try {
            db.beginTransaction();
            db.update(KeyValueTable.TABLE_NAME, values, where, new String[]{key});
            db.setTransactionSuccessful();
        }finally {
            db.endTransaction();
            dbManagerInstance.closeDatabase();
        }
    }

}