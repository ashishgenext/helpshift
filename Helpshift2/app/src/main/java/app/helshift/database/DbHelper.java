package app.helshift.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Ashish on 12-06-2016.
 */
public class DbHelper extends SQLiteOpenHelper {
    private static  final int DbVersion = 1;

    public DbHelper(Context context, String name){
        super(context,name,null,DbVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        try{
            db.execSQL(KeyValueTable.TABLE_SQL);
            db.setTransactionSuccessful();
        }finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
