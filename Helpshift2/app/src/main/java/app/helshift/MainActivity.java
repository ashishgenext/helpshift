package app.helshift;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import app.helshift.database.DbOperations;

/**
 * Created by Ashish on 12-06-2016.
 */
public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DbOperations.insertQuery("hello", "how are you");
        String value = DbOperations.readQuery("hello");

        Log.d("debug", "log value =" + value);
        DbOperations.updateQuery("hello", "I am ashish");
         value = DbOperations.readQuery("hello");

        Log.d("debug", "log value =" + value);

        DbOperations.deleteQuery("hello");
        value = DbOperations.readQuery("hello");

        Log.d("debug", "log value =" + value);



    }
}
