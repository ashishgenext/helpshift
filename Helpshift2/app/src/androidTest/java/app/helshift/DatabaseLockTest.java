package app.helshift;

import android.database.sqlite.SQLiteDatabase;
import android.test.suitebuilder.annotation.Suppress;
import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;
import android.test.AndroidTestCase;
import app.helshift.database.DbHelper;
import app.helshift.database.DbManager;
import app.helshift.database.DbOperations;

@Suppress
public class DatabaseLockTest extends AndroidTestCase {

    private static final int NUM_ITERATIONS = 100;
    private static final int SLEEP_TIME = 30;

    private SQLiteDatabase mDatabase;
    private File mDatabaseFile;
    private DbManager mDbManagerInstance;


    @Override
    protected void setUp() throws Exception {
        super.setUp();
        File parentDir = getContext().getFilesDir();
        mDatabaseFile = new File(parentDir, "myDb.db");

        if (mDatabaseFile.exists()) {
            mDatabaseFile.delete();
        }
        DbManager.initializeInstance(new DbHelper(getContext(), "mydb.db"));
        mDbManagerInstance = DbManager.getInstance();
        mDatabase = mDbManagerInstance.openDatabase();
        assertNotNull(mDatabase);
    }

    @Override
    protected void tearDown() throws Exception {
        mDbManagerInstance.closeDatabase();
        mDatabaseFile.delete();
        super.tearDown();
    }

    @Suppress
    public void testLockFairness() {
        startDatabaseFairnessThread();
        int previous = 0;
        for (int i = 0; i < NUM_ITERATIONS; i++) {
            assertTrue(DbOperations.readQuery(i+"") == "value"+i);
            try {
                Thread.currentThread().sleep(SLEEP_TIME);
            } catch (InterruptedException e) {

            }

        }
    }

    @Suppress
    public void testCrud(){
        for(int i =0;i < NUM_ITERATIONS ;i++){
            DbOperations.insertQuery("key"+i,"value"+i);
            assertTrue((DbOperations.readQuery("key"+i)).equals("value"+i));
        }

        for(int i =0;i < NUM_ITERATIONS ;i++){
            DbOperations.updateQuery("key" + i, "Newvalue" + i);
            assertTrue((DbOperations.readQuery("key"+i)).equals("Newvalue"+i));
        }

        for(int i =0;i < NUM_ITERATIONS ;i++){
            DbOperations.deleteQuery("key"+i);
            assertTrue((DbOperations.readQuery("key"+i)).equals(""));
        }

    }

    private void startDatabaseFairnessThread() {
        Thread thread = new DatabaseFairnessThread();
        thread.start();
    }

    private class DatabaseFairnessThread extends Thread {
        @Override
        public void run() {
            for (int i = 0; i < NUM_ITERATIONS; i++) {
                try {
                    Thread.currentThread().sleep(SLEEP_TIME);
                } catch (InterruptedException e) {
                }

                DbOperations.insertQuery(i+"","value"+i);

            }
        }
    }



}
