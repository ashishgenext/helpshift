import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class NumberFactors {

	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int minRange = Integer.parseInt(br.readLine());
		int maxRange = Integer.parseInt(br.readLine());
		int number = Integer.parseInt(br.readLine());
		
		++minRange ;
		--maxRange;
		
		int maxDiv = maxRange/number ;
		int minDiv = minRange/number ;
		if(minRange%number != 0){
			minDiv++;
		}
		
		for(int i = maxDiv;i>=minDiv;i--){
			System.out.println(number*i);
		}
	}
	
}
